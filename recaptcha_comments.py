from django.contrib.comments.forms import CommentForm
from captcha.fields import ReCaptchaField

class CommentFormWithCaptcha(CommentForm):
    captcha = ReCaptchaField()

def get_form():
    return  CommentFormWithCaptcha
