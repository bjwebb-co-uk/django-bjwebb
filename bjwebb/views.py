from models import Project, Post, Event
from django.views.generic import DetailView, ListView

class ModalDetailView(DetailView):
    def get_context_data(self, **kwargs):
        context = super(ModalDetailView, self).get_context_data(**kwargs)
        context['post_comment'] = 'post_comment' in self.request.GET 
        return context

class SlugView(ModalDetailView):
    @classmethod
    def as_view(cls, *args, **kwargs):
        view = super(SlugView, cls).as_view(*args, **kwargs)
        def useslug(request, slug):
            return view(request, slug=slug)
        return useslug 

class ProjectSubView(ModalDetailView):
    def get_object(self):
        return self.model.objects.get(slug=self.args[1], project__slug=self.args[0])

projects = ListView.as_view(model=Project)
project = SlugView.as_view(model=Project)

posts = ListView.as_view(model=Post)
post = ProjectSubView.as_view(model=Post)

events = ListView.as_view(model=Event)
event = ProjectSubView.as_view(model=Event)

